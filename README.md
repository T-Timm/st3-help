## 使用帮助 ##
--------------------------------------------------------------------



#### 1、下载软件
[Windows安装版](https://download.sublimetext.com/Sublime%20Text%20Build%203095%20Setup.exe)

[Windows便携版](https://download.sublimetext.com/Sublime%20Text%20Build%203095.zip)

[Windows 64位安装版](https://download.sublimetext.com/Sublime%20Text%20Build%203095%20x64%20Setup.exe)

[Windows 64位便携版](https://download.sublimetext.com/Sublime%20Text%20Build%203095%20x64.zip)

[汉化包](http://git.timm.so/Tim/ST3-help/raw/master/Default.sublime-package)

---------------------------------------



#### 2、密钥


    ----- BEGIN LICENSE -----
    Michael Barnes
    Single User License
    EA7E-821385
    8A353C41 872A0D5C DF9B2950 AFF6F667
    C458EA6D 8EA3C286 98D1D650 131A97AB
    AA919AEC EF20E143 B361B1E7 4C8B7F04
    B085E65E 2F5F5360 8489D422 FB8FC1AA
    93F6323C FD7F7544 3F39C318 D95E6480
    FCCC7561 8A4A1741 68FA4223 ADCEDE07
    200C25BE DBBC4855 C4CFB774 C5EC138C
    0FEC1CEF D9DCECEC D3A5DAD1 01316C36
    ------ END LICENSE ------
---

#### 3、安装插件
- 按快捷键`Ctrl+~`或者 `View` >> `Show Console` 打开面板
- 然后输入
 
        import urllib.request,os,hashlib; h = '2915d1851351e5ee549c20394736b442' + '8bc59f460fa1548d1514676163dafc88'; pf = 'Package Control.sublime-package'; ipp = sublime.installed_packages_path(); urllib.request.install_opener( urllib.request.build_opener( urllib.request.ProxyHandler()) ); by = urllib.request.urlopen( 'http://packagecontrol.io/' + pf.replace(' ', '%20')).read(); dh = hashlib.sha256(by).hexdigest(); print('Error validating download (got %s instead of %s), please try manual install' % (dh, h)) if dh != h else open(os.path.join( ipp, pf), 'wb' ).write(by)

- `Enter` 确定安装***[Package Control](http://git.timm.so/Tim/ST3-help/raw/master/Package%20Control.sublime-package)*** 包。

- 网络不给力的话也可以直接下载。

- 然后打开 `Preferences` > `Brower Packages`，文件夹网上一级文件夹![](http://i.imgur.com/C28Cl9j.png)    再进入 `Installed Packages`放入 `Brower Packages`

- 重启软件，之后就可以直接用命令安装插件了。

---


##### 插件安装方法

- 快捷键 `Ctrl+Shift+P` 或者 `Tools` >> `Command Palette` 输入 `install`他就会提示了，我输入 `i` 他都提示了，他好聪明的。然后选择 `install package` 就可以选择你需要的插件了。

- 如果提示`time out`之类的，就再来一次，网络不美丽。

***


##### 示例
![](http://i.imgur.com/pUEC4dE.png)  
![](http://i.imgur.com/IZmLECq.png)



---

#### 4、常用插件推荐

- SublimeLinter = 语法纠错
- JsMinifier = 自动压缩js文件
- Sublime CodeIntel = 代码自动提示
- Bracket Highlighter = 代码匹配
- SublimeTmpl = 快速生成文件模板
- ColorPicker = 调色盘
- Clipboard History = 剪贴板历史记录
- SFTP = 文件传输插件
- WordPress = WordPress函数
- PHPTidy = 排版PHP代码
- Emmet = 用过的都知道
- SideBarEnhancements = 侧栏右键功能增强

###### 推荐一款好看的主题

[Seti_UI](http://git.timm.so/Tim/ST3-help/src/master/Seti_UI)  自行看介绍。

![](http://i.imgur.com/Yp1gfl9.png)




